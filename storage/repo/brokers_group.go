package repo

import "time"

type BrokersGroup struct {
	Id          int64
	Name        string
	AdminId     int64
	GroupInfo   string
	PhoneNumber string
	Email       string
	Address     string
	ImageUrl    string
	Level       string
	CreatedAt   time.Time
}

type BrokerInfo struct {
	Id              int64
	FirstName       string
	LastName        string
	Username        string
	ProfileImageUrl string
	Email           string
}

type CreateBrokersGroupRequest struct {
	Name        string
	AdminId     int64
	GroupInfo   string
	PhoneNumber string
	Email       string
	Address     string
}

type GetAllBrokersGroupsRequest struct {
	Page       int64
	Limit      int64
	Search     string
	SortByDate string
}

type GetAllBrokersGroupsResponse struct {
	BrokersGroups []*BrokersGroup
	Count         int64
}

type ChangeBrokerGroup struct {
	Id          int64
	Name        string
	GroupInfo   string
	PhoneNumber string
	Address     string
}

type BrokersGroupImageUploadRequest struct {
	BrokersGroupId int64
	ImageUrl       string
}

type UpdateBrokersGroupLevelRequest struct {
	BrokersGroupId int64
	Level          string
}

type AddMemberRequest struct {
	GroupId  int64
	BrokerId int64
}

type GroupMember struct {
	Id        int64
	GroupId   int64
	BrokerId  int64
	CreatedAt time.Time
}

type GetAllMembers struct {
	Members []*GroupMember
	Count   int64
}

type BrokersGroupStorageI interface {
	AddMember(*AddMemberRequest) error
	GetMembers(int64) (*GetAllMembers, error)
	CreateBrokersGroup(*CreateBrokersGroupRequest) (*BrokersGroup, error)
	GetBrokersGroup(int64) (*BrokersGroup, error)
	GetAllBrokersGroups(*GetAllBrokersGroupsRequest) (*GetAllBrokersGroupsResponse, error)
	UpdateBrokersGroup(*ChangeBrokerGroup) (*BrokersGroup, error)
	UpdateBrokersGroupLevel(*UpdateBrokersGroupLevelRequest) (*BrokersGroup, error)
	UploadBrokersGroupImage(*BrokersGroupImageUploadRequest) (*BrokersGroup, error)
	DeleteBrokersGroup(int64) error
}
