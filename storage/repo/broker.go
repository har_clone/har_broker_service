package repo

import "time"

type BrokerStorageI interface {
	CreateBroker(*CreateBrokerRequest) (*BrokerResponse, error)
	GetBroker(int64) (*BrokerResponse, error)
	GetBrokerByEmail(*string) (*Broker, error)
	GetAllBrokers(*GetAllBrokersRequest) (*GetAllBrokersResponse, error)
	UpdateBroker(*ChangeBroker) (*BrokerResponse, error)
	UpdateBrokerLevel(*SetBrokerLevel) (*BrokerResponse, error)
	BrokerImageUpload(*BrokerImageUploadReq) (*BrokerResponse, error)
	AddCard(*AddCardReq) (*BrokerResponse, error)
	UpdatePassword(*ChangeBrokerPassword) error
	DeleteBroker(int64) error
}

type Broker struct {
	Id              int
	FirstName       string
	LastName        string
	Username        string
	Password        string
	ProfileImageUrl string
	Bio             string
	Email           string
	Gender          string
	PhoneNumber     string
	Type            string
	BrokerGroupId   int64
	Rating          int64
	Level           string
	CardId          int64
	CreatedAt       time.Time
}

type BrokersGroupInfo struct {
	Id          int64
	Name        string
	GroupInfo   string
	PhoneNumber string
	Email       string
	Address     string
	ImageUrl    string
	Level       string
	CreatedAt   time.Time
}

type BrokerResponse struct {
	Id              int
	FirstName       string
	LastName        string
	Username        string
	ProfileImageUrl string
	Bio             string
	Email           string
	Gender          string
	PhoneNumber     string
	Type            string
	BrokerGroupId   int64
	Rating          int64
	Level           string
	CardId          int64
	CreatedAt       time.Time
}

type CreateBrokerRequest struct {
	FirstName   string
	LastName    string
	Username    string
	Password    string
	Bio         string
	Email       string
	PhoneNumber string
	Gender      string
	Type        string
}

type ChangeBroker struct {
	Id          int64
	FirstName   string
	LastName    string
	Username    string
	PhoneNumber string
	Gender      string
	Bio         string
}

type GetAllBrokersRequest struct {
	Limit      int32
	Page       int32
	Search     string
	SortByDate string
}

type GetAllBrokersResponse struct {
	Brokers []*Broker
	Count   int64
}
type ChangeBrokerPassword struct {
	BrokerId    int64
	NewPassword string
}

type BrokerImageUploadReq struct {
	BrokerId int64
	ImageUrl string
}
type SetBrokerLevel struct {
	Id    int64
	Level string
}

type AddCardReq struct {
	BrokerId int64
	CardId   int64
}
