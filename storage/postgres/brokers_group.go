package postgres

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/har_clone/har_broker_service/pkg/utils"
	"gitlab.com/har_clone/har_broker_service/storage/repo"
)

type brokersGroupRepo struct {
	db *sqlx.DB
}

func NewBrokersGroup(db *sqlx.DB) repo.BrokersGroupStorageI {
	return &brokersGroupRepo{
		db: db,
	}
}

func (bgr *brokersGroupRepo) CreateBrokersGroup(req *repo.CreateBrokersGroupRequest) (*repo.BrokersGroup, error) {
	var brokersGroup repo.BrokersGroup
	query := `
		INSERT INTO brokers_groups(
			name,
			admin_id,
			group_info,
			phone_number,
			email,
			address
		)VALUES($1,$2,$3,$4,$5,$6)
		RETURNING
			id,
			name,
			admin_id,
			COALESCE(group_info,'') as group_info,
			COALESCE(phone_number,'') as phone_number,
			email,
			COALESCE(address,'') as address,
			COALESCE(image_url,'') as image_url,
			COALESCE(level,'') as level,
			created_at
	`

	row := bgr.db.QueryRow(
		query,
		utils.NullString(req.Name),
		req.AdminId,
		utils.NullString(req.GroupInfo),
		utils.NullString(req.PhoneNumber),
		utils.NullString(req.Email),
		utils.NullString(req.Address),
	)

	if err := row.Scan(
		&brokersGroup.Id,
		&brokersGroup.Name,
		&brokersGroup.AdminId,
		&brokersGroup.GroupInfo,
		&brokersGroup.PhoneNumber,
		&brokersGroup.Email,
		&brokersGroup.Address,
		&brokersGroup.ImageUrl,
		&brokersGroup.Level,
		&brokersGroup.CreatedAt,
	); err != nil {
		return &repo.BrokersGroup{}, err
	}
	bgr.AddMember(&repo.AddMemberRequest{GroupId: brokersGroup.Id, BrokerId: req.AdminId})
	return &brokersGroup, nil
}

func (bgr *brokersGroupRepo) GetBrokersGroup(req int64) (*repo.BrokersGroup, error) {
	var brokersGroup repo.BrokersGroup

	query := `
		SELECT
			id,
			name,
			admin_id,
			COALESCE(group_info,'') as group_info,
			COALESCE(phone_number,'') as phone_number,
			email,
			COALESCE(address,'') as address,
			COALESCE(image_url,'') as image_url,
			COALESCE(level,'') as level,
			created_at
		from brokers_groups
		WHERE id = $1
	`
	row := bgr.db.QueryRow(query, req)

	if err := row.Scan(
		&brokersGroup.Id,
		&brokersGroup.Name,
		&brokersGroup.AdminId,
		&brokersGroup.GroupInfo,
		&brokersGroup.PhoneNumber,
		&brokersGroup.Email,
		&brokersGroup.Address,
		&brokersGroup.ImageUrl,
		&brokersGroup.Level,
		&brokersGroup.CreatedAt,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return &repo.BrokersGroup{}, nil
		}
		return &repo.BrokersGroup{}, err
	}

	return &brokersGroup, nil
}

func (bgr *brokersGroupRepo) AddMember(req *repo.AddMemberRequest) error {
	query := `INSERT INTO brokers_group_members(
		broker_id, 
		brokers_group_id
	)VALUES($1,$2)`
	effect, err := bgr.db.Exec(query, req.BrokerId, req.GroupId)
	if err != nil {
		return err
	}
	rowsCount, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (bgr *brokersGroupRepo) GetMembers(GroupId int64) (*repo.GetAllMembers, error) {
	var result repo.GetAllMembers

	query := `
	SELECT 
		id,
		broker_id, 
		brokers_group_id,
		created_at
	FROM brokers_group_members
	WHERE brokers_group_id=$1
	`

	rows, err := bgr.db.Query(query, GroupId)
	if err != nil {

		return &repo.GetAllMembers{}, err
	}
	defer rows.Close()
	for rows.Next() {
		var member repo.GroupMember
		if err := rows.Scan(
			&member.Id,
			&member.BrokerId,
			&member.GroupId,
			&member.CreatedAt,
		); err != nil {
			return &repo.GetAllMembers{}, err
		}
		result.Members = append(result.Members, &member)
		result.Count++
	}

	return &result, nil
}

func (bgr *brokersGroupRepo) GetAllBrokersGroups(param *repo.GetAllBrokersGroupsRequest) (*repo.GetAllBrokersGroupsResponse, error) {
	var result repo.GetAllBrokersGroupsResponse
	result.BrokersGroups = make([]*repo.BrokersGroup, 0)

	offset := (param.Page - 1) * param.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", param.Limit, offset)

	filter := " where true "

	if param.Search != "" {
		str := "%" + param.Search + "%"
		filter += fmt.Sprintf(`
			and (name ILIKE '%s' OR group_info ILIKE '%s' OR phone_number ILIKE '%s' 
				OR email ILIKE '%s' OR address ILIKE '%s' OR level ILIKE '%s') `,
			str, str, str, str, str, str,
		)
	}

	if param.SortByDate == "" {
		param.SortByDate = "desc"
	}

	query := `
		SELECT
			id,
			COALESCE(name,'') as name,
			admin_id,
			COALESCE(group_info,'') as group_info,
			COALESCE(phone_number,'') as phone_number,
			email,
			COALESCE(address,'') as address,
			COALESCE(image_url,'') as image_url,
			COALESCE(level,'') as level,
			created_at
		FROM brokers_groups
		` + filter + `
		ORDER BY created_at ` + param.SortByDate + ` ` + limit
	rows, err := bgr.db.Query(query)
	if err != nil {
		return &repo.GetAllBrokersGroupsResponse{}, err
	}
	defer rows.Close()
	for rows.Next() {
		var BrokersGroup repo.BrokersGroup
		if err = rows.Scan(
			&BrokersGroup.Id,
			&BrokersGroup.Name,
			&BrokersGroup.AdminId,
			&BrokersGroup.GroupInfo,
			&BrokersGroup.PhoneNumber,
			&BrokersGroup.Email,
			&BrokersGroup.Address,
			&BrokersGroup.ImageUrl,
			&BrokersGroup.Level,
			&BrokersGroup.CreatedAt,
		); err != nil {
			return &repo.GetAllBrokersGroupsResponse{}, err
		}
		result.BrokersGroups = append(result.BrokersGroups, &BrokersGroup)
		result.Count++
	}
	queryCount := `SELECT count(1) FROM brokers ` + filter
	err = bgr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return &repo.GetAllBrokersGroupsResponse{}, err
	}

	return &result, nil
}

func (bgr *brokersGroupRepo) UpdateBrokersGroup(req *repo.ChangeBrokerGroup) (*repo.BrokersGroup, error) {
	var brokersGroup repo.BrokersGroup
	query := `
		UPDATE brokers_groups SET
			name = $1,
			group_info = $2,
			phone_number = $3,
			address = $4
		where id = $5
		RETURING 
			id,
			name,
			admin_id,
			COALESCE(group_info,'') as group_info,
			COALESCE(phone_number,'') as phone_number,
			email,
			COALESCE(address,'') as address,
			COALESCE(image_url,'') as image_url,
			COALESCE(level,'') as level,
			created_at
	`

	row := bgr.db.QueryRow(
		query,
		req.Name,
		req.GroupInfo,
		req.PhoneNumber,
		req.Address,
		req.Id,
	)

	if err := row.Scan(
		&brokersGroup.Id,
		&brokersGroup.Name,
		&brokersGroup.AdminId,
		&brokersGroup.GroupInfo,
		&brokersGroup.PhoneNumber,
		&brokersGroup.Email,
		&brokersGroup.Address,
		&brokersGroup.ImageUrl,
		&brokersGroup.Level,
		&brokersGroup.CreatedAt,
	); err != nil {
		return &repo.BrokersGroup{}, err
	}

	return &brokersGroup, nil
}

func (bgr *brokersGroupRepo) UploadBrokersGroupImage(req *repo.BrokersGroupImageUploadRequest) (*repo.BrokersGroup, error) {
	var brokersGroup repo.BrokersGroup
	query := `
		UPDATE brokers_groups SET
			image_url = $1
		where id = $2
		RETURNING
			id,
			name,
			admin_id,
			COALESCE(group_info,'') as group_info,
			COALESCE(phone_number,'') as phone_number,
			email,
			COALESCE(address,'') as address,
			COALESCE(image_url,'') as image_url,
			COALESCE(level,'') as level,
			created_at
	`

	row := bgr.db.QueryRow(
		query,
		req.ImageUrl,
		req.BrokersGroupId,
	)

	if err := row.Scan(
		&brokersGroup.Id,
		&brokersGroup.Name,
		&brokersGroup.AdminId,
		&brokersGroup.GroupInfo,
		&brokersGroup.PhoneNumber,
		&brokersGroup.Email,
		&brokersGroup.Address,
		&brokersGroup.ImageUrl,
		&brokersGroup.Level,
		&brokersGroup.CreatedAt,
	); err != nil {
		return &repo.BrokersGroup{}, err
	}

	return &brokersGroup, nil
}

func (bgr *brokersGroupRepo) UpdateBrokersGroupLevel(req *repo.UpdateBrokersGroupLevelRequest) (*repo.BrokersGroup, error) {
	var brokersGroup repo.BrokersGroup
	query := `
		UPDATE brokers_groups SET
			level = $1
		where id = $2
		RETURNING
			id,
			name,
			admin_id,
			COALESCE(group_info,'') as group_info,
			COALESCE(phone_number,'') as phone_number,
			email,
			COALESCE(address,'') as address,
			COALESCE(image_url,'') as image_url,
			COALESCE(level,'') as level,
			created_at
	`

	row := bgr.db.QueryRow(
		query,
		req.Level,
		req.BrokersGroupId,
	)

	if err := row.Scan(
		&brokersGroup.Id,
		&brokersGroup.Name,
		&brokersGroup.AdminId,
		&brokersGroup.GroupInfo,
		&brokersGroup.PhoneNumber,
		&brokersGroup.Email,
		&brokersGroup.Address,
		&brokersGroup.ImageUrl,
		&brokersGroup.Level,
		&brokersGroup.CreatedAt,
	); err != nil {
		return &repo.BrokersGroup{}, err
	}

	return &brokersGroup, nil
}

func (bgr *brokersGroupRepo) DeleteBrokersGroup(req int64) error {
	effect, err := bgr.db.Exec(`delete from brokers_groups where id=$1`, req)
	if err != nil {
		return err
	}
	rowsCount, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	return nil
}
