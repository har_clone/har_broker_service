package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/har_clone/har_broker_service/pkg/utils"
	"gitlab.com/har_clone/har_broker_service/storage/repo"
)

type brokerRepo struct {
	db *sqlx.DB
}

func NewBroker(db *sqlx.DB) repo.BrokerStorageI {
	return &brokerRepo{
		db: db,
	}
}

func (br *brokerRepo) CreateBroker(req *repo.CreateBrokerRequest) (*repo.BrokerResponse, error) {
	var Broker repo.BrokerResponse
	query := `
		INSERT INTO brokers(
			first_name,
			last_name,
			username,
			bio,
			password,
			email,
			phone_number,
			gender,
			type
		)values($1,$2,$3,$4,$5,$6,$7,$8,$9)
		RETURNING 
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			COALESCE(broker_group_id,0) as broker_group_id,
			COALESCE(rating,0) as rating,
			COALESCE(level,'') as level,
			COALESCE(card_id,0) as card_id,
			created_at
	`

	row := br.db.QueryRow(
		query,
		req.FirstName,
		utils.NullString(req.LastName),
		utils.NullString(req.Username),
		utils.NullString(req.Bio),
		req.Password,
		req.Email,
		utils.NullString(req.PhoneNumber),
		req.Gender,
		req.Type,
	)

	if err := row.Scan(
		&Broker.Id,
		&Broker.FirstName,
		&Broker.LastName,
		&Broker.Username,
		&Broker.ProfileImageUrl,
		&Broker.Bio,
		&Broker.Email,
		&Broker.Gender,
		&Broker.PhoneNumber,
		&Broker.Type,
		&Broker.BrokerGroupId,
		&Broker.Rating,
		&Broker.Level,
		&Broker.CardId,
		&Broker.CreatedAt,
	); err != nil {
		return &repo.BrokerResponse{}, err
	}

	return &Broker, nil
}

func (ur *brokerRepo) GetBroker(Id int64) (*repo.BrokerResponse, error) {
	var Broker repo.BrokerResponse
	query := `
		SELECT 
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			COALESCE(broker_group_id,0) as broker_group_id,
			COALESCE(rating,0) as rating,
			COALESCE(level,'') as level,
			COALESCE(card_id,0) as card_id,
			created_at
		FROM brokers 
		WHERE id=$1
	`

	row := ur.db.QueryRow(query, Id)
	if err := row.Scan(
		&Broker.Id,
		&Broker.FirstName,
		&Broker.LastName,
		&Broker.Username,
		&Broker.ProfileImageUrl,
		&Broker.Bio,
		&Broker.Email,
		&Broker.Gender,
		&Broker.PhoneNumber,
		&Broker.Type,
		&Broker.BrokerGroupId,
		&Broker.Rating,
		&Broker.Level,
		&Broker.CardId,
		&Broker.CreatedAt,
	); err != nil {
		return &repo.BrokerResponse{}, err
	}

	return &Broker, nil
}

func (br *brokerRepo) GetAllBrokers(param *repo.GetAllBrokersRequest) (*repo.GetAllBrokersResponse, error) {
	Result := repo.GetAllBrokersResponse{
		Brokers: make([]*repo.Broker, 0),
	}

	offset := (param.Page - 1) * param.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", param.Limit, offset)

	filter := fmt.Sprintf("where type like '%s' ", "broker")

	if param.Search != "" {
		str := "%" + param.Search + "%"
		filter += fmt.Sprintf(`
			and (first_name ILIKE '%s' OR last_name ILIKE '%s' OR email ILIKE '%s' 
				OR username ILIKE '%s' OR phone_number ILIKE '%s') `,
			str, str, str, str, str,
		)
	}

	if param.SortByDate == "" {
		param.SortByDate = "desc"
	}

	query := `
		SELECT
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			COALESCE(broker_group_id,0) as broker_group_id,
			COALESCE(rating,0) as rating,
			COALESCE(level,'') as level,
			COALESCE(card_id,0) as card_id,
			created_at
		FROM brokers
		` + filter + `
		ORDER BY created_at ` + param.SortByDate + ` ` + limit

	rows, err := br.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var broker repo.Broker

		err := rows.Scan(
			&broker.Id,
			&broker.FirstName,
			&broker.LastName,
			&broker.Username,
			&broker.ProfileImageUrl,
			&broker.Bio,
			&broker.Email,
			&broker.Gender,
			&broker.PhoneNumber,
			&broker.Type,
			&broker.BrokerGroupId,
			&broker.Rating,
			&broker.Level,
			&broker.CardId,
			&broker.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		Result.Brokers = append(Result.Brokers, &broker)
	}

	queryCount := `SELECT count(1) FROM brokers ` + filter
	err = br.db.QueryRow(queryCount).Scan(&Result.Count)
	if err != nil {
		return nil, err
	}

	return &Result, nil
}

func (br *brokerRepo) BrokerImageUpload(req *repo.BrokerImageUploadReq) (*repo.BrokerResponse, error) {
	var Broker repo.BrokerResponse
	query := `
		UPDATE brokers SET 
			profile_image_url=$1
		where id=$2
		RETURNING
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			COALESCE(broker_group_id,0) as broker_group_id,
			COALESCE(rating,0) as rating,
			COALESCE(level,'') as level,
			COALESCE(card_id,0) as card_id,
			created_at
	`

	row := br.db.QueryRow(
		query,
		req.ImageUrl,
		req.BrokerId,
	)

	if err := row.Scan(
		&Broker.Id,
		&Broker.FirstName,
		&Broker.LastName,
		&Broker.Username,
		&Broker.ProfileImageUrl,
		&Broker.Bio,
		&Broker.Email,
		&Broker.Gender,
		&Broker.PhoneNumber,
		&Broker.Type,
		&Broker.BrokerGroupId,
		&Broker.Rating,
		&Broker.Level,
		&Broker.CardId,
		&Broker.CreatedAt,
	); err != nil {
		return &repo.BrokerResponse{}, err
	}

	return &Broker, nil
}

func (br *brokerRepo) UpdateBroker(broker *repo.ChangeBroker) (*repo.BrokerResponse, error) {
	var Broker repo.BrokerResponse
	query := `
		UPDATE brokers SET 
			first_name=$1,
			last_name=$2,
			username=$3,
			phone_number=$4,
			gender=$5,
			bio=$6
		where id=$7
		RETURNING	
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			COALESCE(broker_group_id,0) as broker_group_id,
			COALESCE(rating,0) as rating,
			COALESCE(level,'') as level,
			COALESCE(card_id,0) as card_id,
			created_at
	`

	row := br.db.QueryRow(
		query,
		broker.FirstName,
		utils.NullString(broker.LastName),
		utils.NullString(broker.Username),
		utils.NullString(broker.PhoneNumber),
		broker.Gender,
		broker.Bio,
		broker.Id,
	)
	if err := row.Scan(
		&Broker.Id,
		&Broker.FirstName,
		&Broker.LastName,
		&Broker.Username,
		&Broker.ProfileImageUrl,
		&Broker.Bio,
		&Broker.Email,
		&Broker.Gender,
		&Broker.PhoneNumber,
		&Broker.Type,
		&Broker.BrokerGroupId,
		&Broker.Rating,
		&Broker.Level,
		&Broker.CardId,
		&Broker.CreatedAt,
	); err != nil {
		return &repo.BrokerResponse{}, err
	}

	return &Broker, nil
}

func (br *brokerRepo) UpdateBrokerLevel(req *repo.SetBrokerLevel) (*repo.BrokerResponse, error) {
	var Broker repo.BrokerResponse
	if req.Level != "silver" || req.Level != "gold" || req.Level != "platinum" {
		req.Level = "silver"
	}

	query := `
		UPDATE brokers SET 
			level=$1
		where id=$2
		RETURNING	
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			COALESCE(broker_group_id,0) as broker_group_id,
			COALESCE(rating,0) as rating,
			COALESCE(level,'') as level,
			COALESCE(card_id,0) as card_id,
			created_at
	`

	row := br.db.QueryRow(
		query,
		req.Level,
		req.Id,
	)
	if err := row.Scan(
		&Broker.Id,
		&Broker.FirstName,
		&Broker.LastName,
		&Broker.Username,
		&Broker.ProfileImageUrl,
		&Broker.Bio,
		&Broker.Email,
		&Broker.Gender,
		&Broker.PhoneNumber,
		&Broker.Type,
		&Broker.BrokerGroupId,
		&Broker.Rating,
		&Broker.Level,
		&Broker.CardId,
		&Broker.CreatedAt,
	); err != nil {
		return &repo.BrokerResponse{}, err
	}

	return &Broker, nil
}

func (br *brokerRepo) AddCard(req *repo.AddCardReq) (*repo.BrokerResponse, error) {
	var Broker repo.BrokerResponse

	query := `
		UPDATE brokers SET 
			card_id=$1
		where id=$2
		RETURNING	
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			COALESCE(broker_group_id,0) as broker_group_id,
			COALESCE(rating,0) as rating,
			COALESCE(level,'') as level,
			COALESCE(card_id,0) as card_id,
			created_at
	`

	row := br.db.QueryRow(
		query,
		req.CardId,
		req.BrokerId,
	)
	if err := row.Scan(
		&Broker.Id,
		&Broker.FirstName,
		&Broker.LastName,
		&Broker.Username,
		&Broker.ProfileImageUrl,
		&Broker.Bio,
		&Broker.Email,
		&Broker.Gender,
		&Broker.PhoneNumber,
		&Broker.Type,
		&Broker.BrokerGroupId,
		&Broker.Rating,
		&Broker.Level,
		&Broker.CardId,
		&Broker.CreatedAt,
	); err != nil {
		return &repo.BrokerResponse{}, err
	}

	return &Broker, nil
}

func (ur *brokerRepo) DeleteBroker(ID int64) error {
	effect, err := ur.db.Exec("delete from brokers where id=$1", ID)
	if err != nil {
		return err
	}
	rowsCount, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (ur *brokerRepo) GetBrokerByEmail(email *string) (*repo.Broker, error) {
	var ResponseBroker repo.Broker

	query := `
		SELECT 
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			COALESCE(broker_group_id,0) as broker_group_id,
			COALESCE(rating,0) as rating,
			COALESCE(level,'') as level,
			COALESCE(card_id,0) as card_id,
			created_at
		FROM brokers
		where email=$1
	`

	row := ur.db.QueryRow(query, email)

	if err := row.Scan(
		&ResponseBroker.Id,
		&ResponseBroker.FirstName,
		&ResponseBroker.LastName,
		&ResponseBroker.Username,
		&ResponseBroker.Password,
		&ResponseBroker.ProfileImageUrl,
		&ResponseBroker.Bio,
		&ResponseBroker.Email,
		&ResponseBroker.Gender,
		&ResponseBroker.PhoneNumber,
		&ResponseBroker.Type,
		&ResponseBroker.BrokerGroupId,
		&ResponseBroker.Rating,
		&ResponseBroker.Level,
		&ResponseBroker.CardId,
		&ResponseBroker.CreatedAt,
	); err != nil {
		return nil, err
	}

	return &ResponseBroker, nil
}

func (ur *brokerRepo) UpdatePassword(newPassword *repo.ChangeBrokerPassword) error {
	query := `
		update brokers set
			password=$1
		where id=$2
	`
	effect, err := ur.db.Exec(query, newPassword.NewPassword, newPassword.BrokerId)
	if err != nil {
		return err
	}
	rowsCount, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	return nil
}
