package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/har_clone/har_broker_service/storage/postgres"
	"gitlab.com/har_clone/har_broker_service/storage/repo"
)

type StorageI interface {
	Broker() repo.BrokerStorageI
	BrokersGroup() repo.BrokersGroupStorageI
}

type storagePg struct {
	brokerRepo       repo.BrokerStorageI
	brokersGroupRepo repo.BrokersGroupStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		brokerRepo:       postgres.NewBroker(db),
		brokersGroupRepo: postgres.NewBrokersGroup(db),
	}
}

func (s *storagePg) Broker() repo.BrokerStorageI {
	return s.brokerRepo
}
func (s *storagePg) BrokersGroup() repo.BrokersGroupStorageI {
	return s.brokersGroupRepo
}
