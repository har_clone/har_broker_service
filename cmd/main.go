package main

import (
	"fmt"
	"log"
	"net"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	pb "gitlab.com/har_clone/har_broker_service/genproto/broker_service"
	pbg "gitlab.com/har_clone/har_broker_service/genproto/brokers_group_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/har_clone/har_broker_service/config"
	"gitlab.com/har_clone/har_broker_service/service"
	"gitlab.com/har_clone/har_broker_service/storage"

	grpcPkg "gitlab.com/har_clone/har_broker_service/pkg/grpc_client"
)

func main() {
	cfg := config.Load(".")

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	strg := storage.NewStoragePg(psqlConn)

	grpcConn, err := grpcPkg.New(cfg)
	if err != nil {
		log.Fatalf("failed to get grpc connections: %v", err)
	}

	brokerService := service.NewBrokerService(strg, grpcConn)
	brokersGroupService := service.NewBrokersGroupService(strg, grpcConn)
	lis, err := net.Listen("tcp", cfg.GrpcPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	reflection.Register(s)

	pb.RegisterBrokerServiceServer(s, brokerService)
	pbg.RegisterBrokersGroupServiceServer(s, brokersGroupService)
	log.Println("Grpc server started in port ", cfg.GrpcPort)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}

}
