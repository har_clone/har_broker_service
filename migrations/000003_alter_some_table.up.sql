CREATE table if not exists "brokers_group_members"(
    "id" serial primary key,
    "brokers_group_id" integer not null,
    "broker_id" integer not null UNIQUE,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);