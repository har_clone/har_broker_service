CREATE table if not exists "brokers"(
    "id" serial primary key,
    "first_name" varchar(255) not null,
    "last_name" varchar(255) ,
    "username" varchar(255) UNIQUE,
    "password" varchar(255) not null,
    "profile_image_url" varchar(255),
    "bio" varchar(255),
    "email" varchar(255) UNIQUE,
    "gender" varchar(255),
    "phone_number" varchar(255),
    "type" varchar(255),
    "broker_group_id" integer,
    "rating" integer,
    "level" varchar(255),
    "card_id" integer,
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);