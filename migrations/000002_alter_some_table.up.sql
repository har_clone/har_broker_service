CREATE table if not exists "brokers_groups"(
    "id" serial primary key,
    "name" varchar(255) not null,
    "admin_id" integer,
    "group_info" varchar(255),
    "phone_number" varchar(255),
    "email" varchar(255)UNIQUE,
    "address" varchar(255),
    "image_url" varchar(255),
    "level" varchar(255),
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);