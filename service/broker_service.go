package service

import (
	"context"
	"errors"
	"time"

	pb "gitlab.com/har_clone/har_broker_service/genproto/broker_service"
	grpcPkg "gitlab.com/har_clone/har_broker_service/pkg/grpc_client"
	"gitlab.com/har_clone/har_broker_service/pkg/utils"
	"gitlab.com/har_clone/har_broker_service/storage"
	"gitlab.com/har_clone/har_broker_service/storage/repo"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

const (
	RegisterCodeKey     = "register_code_"
	ForgotPasswordKey   = "forgot_password_code_"
	IsNotStrongPassword = "please be at least 8 characters long. Enter uppercase and lowercase letters, symbols, and numbers"
)

type BrokerService struct {
	pb.UnimplementedBrokerServiceServer
	storage    storage.StorageI
	grpcClient grpcPkg.GrpcClientI
}

func NewBrokerService(strg storage.StorageI, grpcConn grpcPkg.GrpcClientI) *BrokerService {
	return &BrokerService{
		UnimplementedBrokerServiceServer: pb.UnimplementedBrokerServiceServer{},
		storage:                          strg,
		grpcClient:                       grpcConn,
	}
}

func (s *BrokerService) CreateBroker(ctx context.Context, req *pb.CreateBrokerRequest) (*pb.BrokerResponse, error) {
	if !utils.IsStrongPassword(req.Password) {
		return &pb.BrokerResponse{}, errors.New(IsNotStrongPassword)
	}
	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		return &pb.BrokerResponse{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	broker, err := s.storage.Broker().CreateBroker(&repo.CreateBrokerRequest{
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    req.Username,
		Password:    hashedPassword,
		Bio:         req.Bio,
		Email:       req.Email,
		PhoneNumber: req.PhoneNumber,
		Gender:      req.Gender,
		Type:        req.Type,
	})
	if err != nil {
		return &pb.BrokerResponse{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	brokerGroupInfo, err := s.storage.BrokersGroup().GetBrokersGroup(broker.BrokerGroupId)
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	return &pb.BrokerResponse{
		Id:              int64(broker.Id),
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,
		BrokerGroupInfo: &pb.BrokersGroupInfo{
			Id:          brokerGroupInfo.Id,
			Name:        brokerGroupInfo.Name,
			GroupInfo:   brokerGroupInfo.GroupInfo,
			PhoneNumber: brokerGroupInfo.PhoneNumber,
			Email:       brokerGroupInfo.Email,
			Address:     brokerGroupInfo.Address,
			ImageUrl:    brokerGroupInfo.ImageUrl,
			Level:       brokerGroupInfo.Level,
			CreatedAt:   brokerGroupInfo.CreatedAt.Format(time.RFC3339),
		},
		Rating:    float32(broker.Rating),
		Level:     broker.Level,
		CardId:    broker.CardId,
		CreatedAt: broker.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *BrokerService) GetBroker(ctx context.Context, req *pb.BrokerIdRequest) (*pb.BrokerResponse, error) {
	broker, err := s.storage.Broker().GetBroker(req.Id)
	if err != nil {
		return &pb.BrokerResponse{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	brokerGroupInfo, err := s.storage.BrokersGroup().GetBrokersGroup(broker.BrokerGroupId)
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	return &pb.BrokerResponse{
		Id:              int64(broker.Id),
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,
		BrokerGroupInfo: &pb.BrokersGroupInfo{
			Id:          brokerGroupInfo.Id,
			Name:        brokerGroupInfo.Name,
			GroupInfo:   brokerGroupInfo.GroupInfo,
			PhoneNumber: brokerGroupInfo.PhoneNumber,
			Email:       brokerGroupInfo.Email,
			Address:     brokerGroupInfo.Address,
			ImageUrl:    brokerGroupInfo.ImageUrl,
			Level:       brokerGroupInfo.Level,
			CreatedAt:   brokerGroupInfo.CreatedAt.Format(time.RFC3339),
		},
		Rating:    float32(broker.Rating),
		Level:     broker.Level,
		CardId:    broker.CardId,
		CreatedAt: broker.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *BrokerService) GetAllBrokers(ctx context.Context, req *pb.GetAllBrokersRequest) (*pb.GetAllBrokersResponse, error) {
	brokers, err := s.storage.Broker().GetAllBrokers(&repo.GetAllBrokersRequest{
		Limit:      req.Limit,
		Page:       req.Page,
		Search:     req.Search,
		SortByDate: req.SortByDate,
	})
	if err != nil {
		return &pb.GetAllBrokersResponse{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}
	ls := pb.GetAllBrokersResponse{}
	for _, i := range brokers.Brokers {
		ls.Brokers = append(ls.Brokers, &pb.Broker{
			Id:              int64(i.Id),
			FirstName:       i.FirstName,
			LastName:        i.LastName,
			Username:        i.Username,
			ProfileImageUrl: i.ProfileImageUrl,
			Bio:             i.Bio,
			Email:           i.Email,
			Gender:          i.Gender,
			PhoneNumber:     i.PhoneNumber,
			Type:            i.Type,
			BrokerGroupId:   i.BrokerGroupId,
			Rating:          float32(i.Rating),
			Level:           i.Level,
			CardId:          i.CardId,
			CreatedAt:       i.CreatedAt.Format(time.RFC3339),
		})
	}
	ls.Count = brokers.Count
	return &ls, nil
}

func (s *BrokerService) GetBrokerByEmail(ctx context.Context, req *pb.BrokerEmail) (*pb.Broker, error) {
	broker, err := s.storage.Broker().GetBrokerByEmail(&req.Email)
	if err != nil {
		return &pb.Broker{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return &pb.Broker{
		Id:              int64(broker.Id),
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		Password:        broker.Password,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,
		BrokerGroupId:   int64(broker.BrokerGroupId),
		Rating:          float32(broker.Rating),
		Level:           broker.Level,
		CardId:          broker.CardId,
		CreatedAt:       broker.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *BrokerService) UpdateBroker(ctx context.Context, req *pb.ChangeBroker) (*pb.BrokerResponse, error) {
	broker, err := s.storage.Broker().UpdateBroker(&repo.ChangeBroker{
		Id:          req.Id,
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    req.Username,
		PhoneNumber: req.PhoneNumber,
		Gender:      req.Gender,
		Bio:         req.Bio,
	})
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	brokerGroupInfo, err := s.storage.BrokersGroup().GetBrokersGroup(broker.BrokerGroupId)
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	return &pb.BrokerResponse{
		Id:              int64(broker.Id),
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,
		BrokerGroupInfo: &pb.BrokersGroupInfo{
			Id:          brokerGroupInfo.Id,
			Name:        brokerGroupInfo.Name,
			GroupInfo:   brokerGroupInfo.GroupInfo,
			PhoneNumber: brokerGroupInfo.PhoneNumber,
			Email:       brokerGroupInfo.Email,
			Address:     brokerGroupInfo.Address,
			ImageUrl:    brokerGroupInfo.ImageUrl,
			Level:       brokerGroupInfo.Level,
			CreatedAt:   brokerGroupInfo.CreatedAt.Format(time.RFC3339),
		},
		Rating:    float32(broker.Rating),
		Level:     broker.Level,
		CardId:    broker.CardId,
		CreatedAt: broker.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *BrokerService) UpdateBrokerLevel(ctx context.Context, req *pb.SetBrokerLevel) (*pb.BrokerResponse, error) {
	broker, err := s.storage.Broker().UpdateBrokerLevel(&repo.SetBrokerLevel{
		Id:    req.Id,
		Level: req.Level,
	})
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	brokerGroupInfo, err := s.storage.BrokersGroup().GetBrokersGroup(broker.BrokerGroupId)
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	return &pb.BrokerResponse{
		Id:              int64(broker.Id),
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,
		BrokerGroupInfo: &pb.BrokersGroupInfo{
			Id:          brokerGroupInfo.Id,
			Name:        brokerGroupInfo.Name,
			GroupInfo:   brokerGroupInfo.GroupInfo,
			PhoneNumber: brokerGroupInfo.PhoneNumber,
			Email:       brokerGroupInfo.Email,
			Address:     brokerGroupInfo.Address,
			ImageUrl:    brokerGroupInfo.ImageUrl,
			Level:       brokerGroupInfo.Level,
			CreatedAt:   brokerGroupInfo.CreatedAt.Format(time.RFC3339),
		},
		Rating:    float32(broker.Rating),
		Level:     broker.Level,
		CardId:    broker.CardId,
		CreatedAt: broker.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *BrokerService) AddCard(ctx context.Context, req *pb.AddCardReq) (*pb.BrokerResponse, error) {
	broker, err := s.storage.Broker().AddCard(&repo.AddCardReq{
		BrokerId: req.BrokerId,
		CardId:   req.CardId,
	})
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	brokerGroupInfo, err := s.storage.BrokersGroup().GetBrokersGroup(broker.BrokerGroupId)
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	return &pb.BrokerResponse{
		Id:              int64(broker.Id),
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,
		BrokerGroupInfo: &pb.BrokersGroupInfo{
			Id:          brokerGroupInfo.Id,
			Name:        brokerGroupInfo.Name,
			GroupInfo:   brokerGroupInfo.GroupInfo,
			PhoneNumber: brokerGroupInfo.PhoneNumber,
			Email:       brokerGroupInfo.Email,
			Address:     brokerGroupInfo.Address,
			ImageUrl:    brokerGroupInfo.ImageUrl,
			Level:       brokerGroupInfo.Level,
			CreatedAt:   brokerGroupInfo.CreatedAt.Format(time.RFC3339),
		},
		Rating:    float32(broker.Rating),
		Level:     broker.Level,
		CardId:    broker.CardId,
		CreatedAt: broker.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *BrokerService) BrokerImageUpload(ctx context.Context, req *pb.BrokerImageUploadReq) (*pb.BrokerResponse, error) {
	broker, err := s.storage.Broker().BrokerImageUpload(&repo.BrokerImageUploadReq{
		BrokerId: req.BrokerId,
		ImageUrl: req.ImageUrl,
	})
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	brokerGroupInfo, err := s.storage.BrokersGroup().GetBrokersGroup(broker.BrokerGroupId)
	if err != nil {
		return &pb.BrokerResponse{}, err
	}
	return &pb.BrokerResponse{
		Id:              int64(broker.Id),
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,
		BrokerGroupInfo: &pb.BrokersGroupInfo{
			Id:          brokerGroupInfo.Id,
			Name:        brokerGroupInfo.Name,
			GroupInfo:   brokerGroupInfo.GroupInfo,
			PhoneNumber: brokerGroupInfo.PhoneNumber,
			Email:       brokerGroupInfo.Email,
			Address:     brokerGroupInfo.Address,
			ImageUrl:    brokerGroupInfo.ImageUrl,
			Level:       brokerGroupInfo.Level,
			CreatedAt:   brokerGroupInfo.CreatedAt.Format(time.RFC3339),
		},
		Rating:    float32(broker.Rating),
		Level:     broker.Level,
		CardId:    broker.CardId,
		CreatedAt: broker.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *BrokerService) UpdatePassword(ctx context.Context, req *pb.ChangeBrokerPassword) (*emptypb.Empty, error) {
	err := s.storage.Broker().UpdatePassword(&repo.ChangeBrokerPassword{
		BrokerId:    req.BrokerId,
		NewPassword: req.NewPassword,
	})
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}

func (s *BrokerService) DeleteBroker(ctx context.Context, req *pb.BrokerIdRequest) (*emptypb.Empty, error) {
	err := s.storage.Broker().DeleteBroker(req.Id)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}
