package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	pb "gitlab.com/har_clone/har_broker_service/genproto/brokers_group_service"
	grpcPkg "gitlab.com/har_clone/har_broker_service/pkg/grpc_client"
	"gitlab.com/har_clone/har_broker_service/storage"
	"gitlab.com/har_clone/har_broker_service/storage/repo"
	"google.golang.org/protobuf/types/known/emptypb"
)

type BrokersGroupService struct {
	pb.BrokersGroupServiceServer
	storage    storage.StorageI
	grpcClient grpcPkg.GrpcClientI
}

func NewBrokersGroupService(strg storage.StorageI, grpcConn grpcPkg.GrpcClientI) *BrokersGroupService {
	return &BrokersGroupService{
		storage:    strg,
		grpcClient: grpcConn,
	}
}

func (bg *BrokersGroupService) CreateBrokersGroup(ctx context.Context, req *pb.CreateBrokersGroupRequest) (*pb.BrokersGroupResponse, error) {
	brokersGroup, err := bg.storage.BrokersGroup().CreateBrokersGroup(&repo.CreateBrokersGroupRequest{
		Name:        req.Name,
		AdminId:     req.AdminId,
		GroupInfo:   req.GroupInfo,
		PhoneNumber: req.PhoneNumber,
		Email:       req.Email,
		Address:     req.Address,
	})
	if err != nil {
		return &pb.BrokersGroupResponse{}, err
	}
	brokerInfo, err := bg.storage.Broker().GetBroker(brokersGroup.AdminId)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &pb.BrokersGroupResponse{}, err
		}
	}
	return &pb.BrokersGroupResponse{
		Id:          brokersGroup.Id,
		Name:        brokersGroup.Name,
		GroupInfo:   brokersGroup.GroupInfo,
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		AdminInfo: &pb.BrokerInfo{
			Id:              int64(brokerInfo.Id),
			FirstName:       brokerInfo.FirstName,
			LastName:        brokerInfo.LastName,
			Username:        brokerInfo.Username,
			ProfileImageUrl: brokerInfo.ProfileImageUrl,
			Email:           brokerInfo.Email,
		},
		ImageUrl:  brokersGroup.ImageUrl,
		Level:     brokersGroup.Level,
		CreatedAt: brokersGroup.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (bg *BrokersGroupService) GetBrokersGroup(ctx context.Context, req *pb.BrokersGroupIdRequest) (*pb.BrokersGroupResponse, error) {
	brokersGroup, err := bg.storage.BrokersGroup().GetBrokersGroup(int64(req.Id))
	if err != nil {
		return &pb.BrokersGroupResponse{}, err
	}

	brokerInfo, err := bg.storage.Broker().GetBroker(brokersGroup.AdminId)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &pb.BrokersGroupResponse{}, err
		}
	}

	return &pb.BrokersGroupResponse{
		Id:   brokersGroup.Id,
		Name: brokersGroup.Name,
		AdminInfo: &pb.BrokerInfo{
			Id:              int64(brokerInfo.Id),
			FirstName:       brokerInfo.FirstName,
			LastName:        brokerInfo.LastName,
			Username:        brokerInfo.Username,
			ProfileImageUrl: brokerInfo.ProfileImageUrl,
			Email:           brokerInfo.Email,
		},
		GroupInfo:   brokersGroup.GroupInfo,
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (bg *BrokersGroupService) GetAllBrokersGroups(ctx context.Context, req *pb.GetAllBrokersGroupsRequest) (*pb.GetAllBrokersGroupsResponse, error) {
	var result pb.GetAllBrokersGroupsResponse
	brokersGroups, err := bg.storage.BrokersGroup().GetAllBrokersGroups(&repo.GetAllBrokersGroupsRequest{
		Page:       req.Page,
		Limit:      req.Limit,
		Search:     req.Search,
		SortByDate: req.SortByDate,
	})

	if err != nil {
		return &pb.GetAllBrokersGroupsResponse{}, err
	}

	for _, i := range brokersGroups.BrokersGroups {
		result.BrokersGroups = append(result.BrokersGroups, &pb.BrokersGroup{
			Id:          i.Id,
			Name:        i.Name,
			AdminId:     i.AdminId,
			GroupInfo:   i.GroupInfo,
			PhoneNumber: i.PhoneNumber,
			Email:       i.Email,
			Address:     i.Address,
			ImageUrl:    i.ImageUrl,
			Level:       i.Level,
			CreatedAt:   i.CreatedAt.Format(time.RFC3339),
		})
	}
	result.Count = brokersGroups.Count
	return &result, nil
}

func (bg *BrokersGroupService) UpdateBrokersGroup(ctx context.Context, req *pb.ChangeBrokersGroup) (*pb.BrokersGroupResponse, error) {
	brokersGroup, err := bg.storage.BrokersGroup().UpdateBrokersGroup(&repo.ChangeBrokerGroup{
		Id:          req.Id,
		Name:        req.Name,
		GroupInfo:   req.GroupInfo,
		PhoneNumber: req.PhoneNumber,
		Address:     req.Address,
	})
	if err != nil {
		return &pb.BrokersGroupResponse{}, err
	}
	brokerInfo, err := bg.storage.Broker().GetBroker(brokersGroup.AdminId)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &pb.BrokersGroupResponse{}, err
		}
	}
	return &pb.BrokersGroupResponse{
		Id:   brokersGroup.Id,
		Name: brokersGroup.Name,
		AdminInfo: &pb.BrokerInfo{
			Id:              int64(brokerInfo.Id),
			FirstName:       brokerInfo.FirstName,
			LastName:        brokerInfo.LastName,
			Username:        brokerInfo.Username,
			ProfileImageUrl: brokerInfo.ProfileImageUrl,
			Email:           brokerInfo.Email,
		},
		GroupInfo:   brokersGroup.GroupInfo,
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (bg *BrokersGroupService) UploadBrokersGroupImage(ctx context.Context, req *pb.BrokersGroupImageUploadRequest) (*pb.BrokersGroupResponse, error) {
	brokersGroup, err := bg.storage.BrokersGroup().UploadBrokersGroupImage(&repo.BrokersGroupImageUploadRequest{
		BrokersGroupId: req.BrokersGroupId,
		ImageUrl:       req.ImageUrl,
	})
	if err != nil {
		return &pb.BrokersGroupResponse{}, err
	}

	brokerInfo, err := bg.storage.Broker().GetBroker(brokersGroup.AdminId)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &pb.BrokersGroupResponse{}, err
		}
	}
	return &pb.BrokersGroupResponse{
		Id:   brokersGroup.Id,
		Name: brokersGroup.Name,
		AdminInfo: &pb.BrokerInfo{
			Id:              int64(brokerInfo.Id),
			FirstName:       brokerInfo.FirstName,
			LastName:        brokerInfo.LastName,
			Username:        brokerInfo.Username,
			ProfileImageUrl: brokerInfo.ProfileImageUrl,
			Email:           brokerInfo.Email,
		},
		GroupInfo:   brokersGroup.GroupInfo,
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (bg *BrokersGroupService) AddMember(ctx context.Context, req *pb.AddMemberRequest) (*emptypb.Empty, error) {
	err := bg.storage.BrokersGroup().AddMember(&repo.AddMemberRequest{
		BrokerId: req.BrokerId,
		GroupId:  req.GroupId,
	})
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}

func (bg *BrokersGroupService) GetMembers(ctx context.Context, req *pb.BrokersGroupIdRequest) (*pb.GetAllMembers, error) {
	var result pb.GetAllMembers
	resp, err := bg.storage.BrokersGroup().GetMembers(req.Id)
	if err != nil {
		return &pb.GetAllMembers{}, err
	}

	for _, organ := range resp.Members {
		result.Members = append(result.Members, &pb.GroupMember{
			Id:        organ.Id,
			GroupId:   organ.GroupId,
			BrokerId:  organ.BrokerId,
			CreatedAt: organ.CreatedAt.Format(time.RFC3339),
		})
	}
	result.Count = resp.Count

	res, err := bg.storage.BrokersGroup().GetBrokersGroup(req.Id)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &pb.GetAllMembers{}, err
		}
	}

	adminInfo, err := bg.storage.Broker().GetBroker(res.AdminId)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &pb.GetAllMembers{}, err
		}
	}

	result.GroupInfo = &pb.BrokersGroupResponse{
		Id:   res.Id,
		Name: res.Name,
		AdminInfo: &pb.BrokerInfo{
			Id:              int64(adminInfo.Id),
			FirstName:       adminInfo.FirstName,
			LastName:        adminInfo.LastName,
			Username:        adminInfo.Username,
			ProfileImageUrl: adminInfo.ProfileImageUrl,
			Email:           adminInfo.Email,
		},
		GroupInfo:   res.GroupInfo,
		PhoneNumber: res.PhoneNumber,
		Email:       res.Email,
		Address:     res.Address,
		ImageUrl:    res.ImageUrl,
		Level:       res.Level,
		CreatedAt:   res.CreatedAt.Format(time.RFC3339),
	}

	return &result, nil
}

func (bg *BrokersGroupService) UploadBrokersGroupLevel(ctx context.Context, req *pb.UploadBrokersGroupLevelRequest) (*pb.BrokersGroupResponse, error) {
	brokersGroup, err := bg.storage.BrokersGroup().UpdateBrokersGroupLevel(&repo.UpdateBrokersGroupLevelRequest{
		BrokersGroupId: req.BrokersGroupId,
		Level:          req.Level,
	})
	if err != nil {
		return &pb.BrokersGroupResponse{}, err
	}

	brokerInfo, err := bg.storage.Broker().GetBroker(brokersGroup.AdminId)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return &pb.BrokersGroupResponse{}, err
		}
	}
	return &pb.BrokersGroupResponse{
		Id:   brokersGroup.Id,
		Name: brokersGroup.Name,
		AdminInfo: &pb.BrokerInfo{
			Id:              int64(brokerInfo.Id),
			FirstName:       brokerInfo.FirstName,
			LastName:        brokerInfo.LastName,
			Username:        brokerInfo.Username,
			ProfileImageUrl: brokerInfo.ProfileImageUrl,
			Email:           brokerInfo.Email,
		},
		GroupInfo:   brokersGroup.GroupInfo,
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (bg *BrokersGroupService) DeleteBrokersGroup(ctx context.Context, req *pb.BrokersGroupIdRequest) (*emptypb.Empty, error) {
	err := bg.storage.BrokersGroup().DeleteBrokersGroup(int64(req.Id))
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}
